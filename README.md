NodeJS require と nodeunit のテスト


Module-A と Module-B はそれぞれ 1度目の require で実体化する 
```
[NodeJS Context] <- require(<Module-A>)
[NodeJS Context] <- require(<Module-B>)
```


Module-A を実体化し、次に Module-B の実体化するときには Module-A のコンテキストを割り当てる
```
[NodeJS Context] <- require(<Module-A>)
[NodeJS Context] <- require(<Module-B>) < require(<Module-A>)
```
