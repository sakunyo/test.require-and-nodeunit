var hello = require("../lib/hello");


exports.testHello = function(test){
  //*/
  test.expect(1);
  var str = hello.hello("NodeJS"); // update require value
  test.equal(str, "Hello NodeJS");
  //*/
  hello.check();
  test.done();
};